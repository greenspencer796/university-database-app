# Import the Flask app instance from the backend module
from backend import app

# Check if the script is run directly (not imported as a module)
if __name__ == "__main__":
    # Start the Flask application with debugging enabled and on port 5000 
    app.run(debug=True, port=5000, host='0.0.0.0')
