# Third-party libraries
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, BooleanField, DateField, SelectField, IntegerField
from wtforms.validators import DataRequired, Length, NumberRange, ValidationError
from datetime import datetime, timedelta


# The StudentForm class inherits from FlaskForm and is used to create a form for adding or editing student details.
# It contains fields for student name, age, gender, enrollment date, scholarship status, tutor selection, and course selection
# It has relevant validations in place to ensure that the data entered meets specific criteria.
class StudentForm(FlaskForm):
    # The name field is a StringField that expects a student's name with a minimum length of 2 characters and a maximum
    # length of 70 characters. It is a required field.
    name = StringField("Name", validators=[DataRequired(), Length(min=2, max=70)])

    # The age field is an IntegerField that expects a number representing the age of the student.
    # The age should be a number between 15 and 90, inclusive. It is a required field.
    age = IntegerField("Age", validators=[DataRequired(), NumberRange(min=15, max=90, message="Student age must be between 15 and 90 years.")])

    # The gender field is a SelectField that allows users to select a gender from a predefined list of options. It is a required field.
    gender = SelectField("Gender", choices=[("", "Select Gender"), ("Male", "Male"), ("Female", "Female"), ("Non-Binary", "Non-Binary"), ("Other", "Other")],
                         validators=[DataRequired()])

    # The enrollment_date field is a DateField that expects a date representing the enrollment date of the student. It is a required field.
    # It has additional validations defined in the validate_enrollment_date method.
    enrollment_date = DateField("Enrollment Date", validators=[DataRequired()])

    # The scholarship field is a BooleanField that indicates whether the student is on a scholarship. It is not a required field.
    scholarship = BooleanField("Scholarship")

    # The tutor field is a SelectField that allows users to select a tutor from a list of options. It is a required field.
    tutor = SelectField("Tutor", validators=[DataRequired()])

    # The course field is a SelectField that allows users to select a course from a list of options. It is a required field.
    course = SelectField("Course", validators=[DataRequired()])

    # The submit field is a SubmitField that is used to submit the form data. The label text can be changed using the set_submit_text method.
    submit = SubmitField("Add Student")

    # The set_submit_text method is used to change the label text of the submit button.
    # It accepts one parameter, text, which is the new label text to be set.
    def set_submit_text(self, text):
        self.submit.label.text = text

    # The validate_enrollment_date method validates the enrollment_date field.
    # It checks whether the enrollment date is within a valid range (not more than 365 days in the future and not before January 1, 1950).
    def validate_enrollment_date(self, enrollment_date):
        if enrollment_date.data > (datetime.today() + timedelta(days=365)).date():
            raise ValidationError("The enrollment date cannot be more then 365 days in advance")
        if enrollment_date.data < datetime(1950, 1, 1).date():
            raise ValidationError("The enrollment date cannot be past 01/01/1950.")




