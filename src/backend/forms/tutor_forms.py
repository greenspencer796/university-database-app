# Third-party libraries
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length, NumberRange


# The TutorForm class inherits from FlaskForm and is used to create a form for adding or editing tutor details.
# It contains fields for tutor name, age, and gender.
# It has relevant validations in place to ensure that the data entered meets specific criteria.
class TutorForm(FlaskForm):
    # The name field is a StringField that expects a tutor's name with a minimum length of 2 characters and a maximum
    # length of 70 characters. It is a required field.
    name = StringField("Name", validators=[DataRequired(), Length(min=2, max=70)])

    # The age field is an IntegerField that expects a number representing the age of the tutor.
    # The age should be a number between 18 and 90, inclusive. It is a required field.
    age = IntegerField("Age", validators=[DataRequired(), NumberRange(min=18, max=90, message="Tutor age must be between 18 and 90 years.")])

    # The gender field is a SelectField that allows users to select a gender from a predefined list of options. It is a required field.
    gender = SelectField("Gender", choices=[("", "Select Gender"), ("Male", "Male"), ("Female", "Female"), ("Non-Binary", "Non-Binary"), ("Other", "Other")],
                         validators=[DataRequired()])

    # The submit field is a SubmitField that is used to submit the form data. The label text can be changed using the set_submit_text method.
    submit = SubmitField("Add Tutor")

    # The set_submit_text method is used to change the label text of the submit button.
    # It accepts one parameter, text, which is the new label text to be set.
    def set_submit_text(self, text):
        self.submit.label.text = text
