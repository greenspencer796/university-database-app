# Third-party libraries
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, SelectField, DecimalField
from wtforms.validators import DataRequired, Length, ValidationError, NumberRange


# The CourseForm class inherits from FlaskForm and is used to create a form for adding or editing a course.
# It contains fields for course name, length, course fee, and teacher selection
# It has relevant validations in place to ensure that the data entered meets specific criteria.
class CourseForm(FlaskForm):
    # The name field is a StringField that expects a course name with a minimum length of 2 characters and a maximum
    # length of 150 characters. It is a required field.
    name = StringField("Course Name", validators=[DataRequired(), Length(min=2, max=150)])

    # The length field is an IntegerField that expects a number representing the length of the course in years.
    # It should be a number between 1 and 10, inclusive. It is a required field.
    length = IntegerField("Length", validators=[DataRequired(), NumberRange(min=1, max=10, message="Course length must be between 1 and 10 years.")])

    # The course_fee field is a DecimalField that expects a decimal number representing the course fee in GBP.
    # The fee should be between £5,000 and £25,000, inclusive. It is a required field.
    course_fee = DecimalField("Course Fee", validators=[DataRequired(), NumberRange(min=5000, max=25000, message="Course fee must be between £5,000 and £25,000.")])

    # The teacher field is a SelectField that allows users to select a teacher from a list of options. It is a required field.
    teacher = SelectField("Teacher", validators=[DataRequired()])

    # The submit field is a SubmitField that is used to submit the form data. The label text can be changed using the set_submit_text method.
    submit = SubmitField("Add Course")

    # The set_submit_text method is used to change the label text of the submit button.
    # It accepts one parameter, text, which is the new label text to be set.
    def set_submit_text(self, text):
        self.submit.label.text = text
