# Third-party libraries
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError

# Local application modules
from backend.models import User


# The RegistrationForm class inherits from FlaskForm and is utilized to create a registration form for users.
# It includes fields such as email, password, confirm password, and admin access.
# It has relevant validations implemented to guarantee that the input data conforms to specified criteria.
class RegistrationForm(FlaskForm):
    # The email field is a StringField that expects a valid email, and a maximum length of 100 characters. It is a required field.
    email = StringField('Email', validators=[DataRequired(), Email(), Length(max=100)])

    # The password field is a PasswordField that requires a password with a minimum length of 5 characters and a maximum
    # length of 30 characters. It is a required field.
    password = PasswordField('Password', validators=[DataRequired(), Length(min=5, max=30)])

    # The confirm_password field is a PasswordField that expects the password should match the password field above. It is a required field.
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), Length(min=5, max=30), EqualTo("password", message="Passwords do not match.")])

    # The admin field is a BooleanField that indicates whether the user has admin access or not.
    admin = BooleanField("Admin Access")

    # The submit field is a SubmitField that is used to submit the form data.
    submit = SubmitField("Sign Up")

    # The validate_email method is used to validate that the entered email is not already taken in the database.
    def validate_email(self, email):
        email = User.query.filter_by(email=email.data).first()
        if email:
            raise ValidationError("That email is taken.")

    # The validate_password method verifies that the entered password contains at least one lowercase letter, one uppercase letter, and one digit.
    def validate_password(self, password):
        password = password.data
        has_lowercase = any(char.islower() for char in password)
        has_uppercase = any(char.isupper() for char in password)
        has_digit = any(char.isdigit() for char in password)

        if not (has_lowercase and has_uppercase and has_digit):
            raise ValidationError("Password must contain at least one lowercase letter, one uppercase letter, and one digit.")


# The LoginForm class inherits from FlaskForm and is designed to create a login form for users.
# It contains fields for email and password.
# It has relevant validations in place to ensure that the data entered meets specific criteria.
class LoginForm(FlaskForm):
    # The email field is a StringField that expects a valid email, and having a maximum length of 100 characters. It is a required field.
    email = StringField('Email', validators=[DataRequired(), Email(), Length(max=100)])

    # The password field is a PasswordField that requires a password with a minimum length of 5 characters and a
    # maximum length of 30 characters. It is a required field.
    password = PasswordField('Password', validators=[DataRequired(), Length(min=5, max=30)])

    # The submit field is a SubmitField that is used to submit the form data.
    submit = SubmitField("Sign In")
