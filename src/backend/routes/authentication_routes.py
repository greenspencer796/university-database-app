# Third-party libraries
from flask import url_for, redirect, Blueprint
from flask_login import current_user, logout_user

# Local application modules
from backend.forms.authentication_forms import RegistrationForm, LoginForm
from backend.services.authentication_services import create_user_handler, login_user_handler
from backend.responses.authentication_responses import authentication_response

# Initializes a Blueprint object to organize authentication-related routes
authentication = Blueprint('authentication', __name__)


# Route to handle registering a new user.
@authentication.route("/register", methods=["GET", "POST"])
def register():
    """
    This function handles the registration of a new user

    It initializes a form instance to capture registration details.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = RegistrationForm()
    return authentication_response(form, create_user_handler, "Register", "authentication.login", "There has been an error with the database. Please try again.")


# Route to handle logining in a user.
@authentication.route("/login", methods=["GET", "POST"])
def login():
    """
    This function handles the login of a user

    It initializes a form instance to capture login details.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = LoginForm()
    return authentication_response(form, login_user_handler, "Login", "main.home", "Login Unsuccessful. Email or Password is invalid")


# Route to handle logining out a user.
@authentication.route("/logout")
def logout():
    """
    This function handles the logout of a logged-in user

    It logs out a user, using flask's logout function

    Returns:
    response object: A response object that redirects to the login page.
    """
    logout_user()
    return redirect(url_for("authentication.login"))
