# Third-party libraries
from flask import Blueprint
from flask_login import login_required

# Local application modules
from backend.models import Student, Course, Tutor
from backend.forms.student_forms import StudentForm
from backend.responses.common_responses import add_record_response, update_record_response, delete_record_response, \
                                                 display_record_response, display_all_records_response
from backend.utils.common_utils import choices_list_builder
from backend.utils.student_utils import populate_student_form, check_student_records_match

# Initializes a Blueprint object to organize student-related routes
students = Blueprint('students', __name__)


# Route to handle adding new students. Access to this route is restricted to logged-in users.
@students.route("/students/new", methods=["GET", "POST"])
@login_required
def add_student():
    """
    This function handles the addition of a new student.

      It performs the following steps:
        1. Initializes a form instance to capture student details.
        2. Populates the tutor and course choices fields of the form from the Tutor and Course database tables,
           allowing users to select a tutor and course when creating a student.
        3. Computes the current number of tutors and courses in the database to ensure choice lists are managed appropriately.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = StudentForm()
    form.tutor.choices = choices_list_builder(Tutor, "Select Tutor")
    form.course.choices = choices_list_builder(Course, "Select Course")
    number_of_tutors = len(Tutor.query.all())
    number_of_courses = len(Course.query.all())
    return add_record_response(form, Student, "Student", number_of_tutors=number_of_tutors, number_of_courses=number_of_courses)


# Route to handle updating existing students, identified by their unique IDs. Access to this route is restricted to logged-in users.
@students.route("/students/<int:student_id>/edit", methods=["GET", "POST"])
@login_required
def update_student(student_id):
    """
    This function handles the updating of an existing student.

    Parameters:
    student_id (int): The ID of the student to be updated.

      It performs the following steps:
        1. Initializes a form instance to capture updated student details.
        2. Changes the default form submission button text to 'Update Student'.
        3. Populates the tutor and course choices fields of the form from the Tutor and course database tables,
           allowing users to select a tutor and course when updating a student.
        4. Computes the current number of tutors and courses in the database to ensure choice lists are managed appropriately.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = StudentForm()
    form.set_submit_text("Update Student")
    form.tutor.choices = choices_list_builder(Tutor, "Select Tutor")
    form.course.choices = choices_list_builder(Course, "Select Course")
    number_of_tutors = len(Tutor.query.all())
    number_of_courses = len(Course.query.all())
    return update_record_response(form, Student, populate_student_form, check_student_records_match, student_id, "Student",
                                  number_of_tutors, number_of_courses)


# Route to handle the display of individual students, identified by their unique IDs. Access to this route is restricted to logged-in users.
@students.route("/students/<int:student_id>")
@login_required
def display_student(student_id):
    """
    This function handles the display of individual student details.

    Parameters:
    student_id (int): The ID of the student to be displayed.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return display_record_response(Student, student_id, "Student")


# Route to handle the deletion of individual students, identified by their unique IDs. Access to this route is restricted to logged-in users.
@students.route("/students/<int:student_id>/delete", methods=["POST"])
@login_required
def delete_student(student_id):
    """
    This function handles the deletion of an individual student.

    Parameters:
    student_id (int): The ID of the student to be deleted.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return delete_record_response(Student, student_id, "Student")


# Route to handle the display of all students. Access to this route is restricted to logged-in users.
@students.route("/students", methods=["GET", "POST"])
@login_required
def display_all_students():
    """
    This function handles the display of all students.

    It specifies a list of CSS classes to be dynamically resized for a matching display

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    elements_to_dynamically_resize = ['record-name', 'teacher-name']
    return display_all_records_response(Student, "Student", elements_to_dynamically_resize)
