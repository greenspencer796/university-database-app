# Third-party libraries
from flask import render_template, Blueprint
from flask_login import login_required
from backend.utils.common_utils import send_response

# Initializes a Blueprint object to organize main-related routes
main = Blueprint('main', __name__)


# Route to handle the display of the home page. Access to this route is restricted to logged-in users.
@main.route("/")
@main.route("/home")
@login_required
def home():
    """
    This function handles the displaying of the home page.

    It creates a dictionary to populate information cards on the home page. Each key represents a category and the corresponding
    value is the description for that category.

    Returns:
    function: A function to formulate a response with a rendered HTML template and a 200 status code.
    """
    home_page_cards = {
        "Students": "Discover our diligent students, who come from diverse age groups and are enrolled in a variety of courses.",
        "Courses": "Explore our offerings of engaging courses, spanning a broad spectrum of subjects, all designed to captivate your interest.",
        "Teachers": "Meet our dedicated teachers, who tirelessly devote themselves to helping students achieve exceptional grades.",
        "Tutors": "Get to know our tutors, who offer incredible support to assist our students throughout their university journey."
    }
    return send_response(render_template("main/home.html", title="Home", home_page_cards=home_page_cards), 200)


# Route to handle the attempt to access an invalid URL.
@main.route("/<path:path>")
def page_not_found(path):
    """
    This function handles the attempt to access an incorrect URL.

    Parameters:
    path (str): The incorrect path entered by the user.

    Returns:
    function: A function to formulate a response with a rendered HTML template and a 404 status code.
    """
    return send_response(render_template("error/error_page.html", status_code=404, message="An invalid URL has been entered."), 404)
