# Third-party libraries
from flask import Blueprint
from flask_login import login_required

# Local application modules
from backend.models import Tutor
from backend.forms.tutor_forms import TutorForm
from backend.responses.common_responses import add_record_response, update_record_response, delete_record_response, \
                                                 display_record_response, display_all_records_response
from backend.utils.common_utils import check_staff_records_match, populate_staff_form

# Initializes a Blueprint object to organize tutor-related routes
tutors = Blueprint('tutors', __name__)


# Route to handle adding new tutors. Access to this route is restricted to logged-in users.
@tutors.route("/tutors/new", methods=["GET", "POST"])
@login_required
def add_tutor():
    """
    This function handles the addition of a new tutor.

    It initializes a form instance to capture tutor details.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = TutorForm()
    return add_record_response(form, Tutor, "Tutor")


# Route to handle updating existing tutors, identified by their unique IDs. Access to this route is restricted to logged-in users.
@tutors.route("/tutors/<int:tutor_id>/edit", methods=["GET", "POST"])
@login_required
def update_tutor(tutor_id):
    """
    This function handles the updating of an existing tutor.

    Parameters:
    tutor_id (int): The ID of the tutor to be updated.

      It performs the following steps:
        1. Initializes a form instance to capture updated tutor details.
        2. Changes the default form submission button text to 'Update Tutor'.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = TutorForm()
    form.set_submit_text("Update Tutor")
    return update_record_response(form, Tutor, populate_staff_form, check_staff_records_match, tutor_id, "Tutor")


# Route to handle the display of individual tutors, identified by their unique IDs. Access to this route is restricted to logged-in users.
@tutors.route("/tutors/<int:tutor_id>")
@login_required
def display_tutor(tutor_id):
    """
    This function handles the display of individual tutor details.

    Parameters:
    tutor_id (int): The ID of the tutor to be displayed.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return display_record_response(Tutor, tutor_id, "Tutor")


# Route to handle the deletion of individual tutors, identified by their unique IDs. Access to this route is restricted to logged-in users.
@tutors.route("/tutors/<int:tutor_id>/delete", methods=["POST"])
@login_required
def delete_tutor(tutor_id):
    """
    This function handles the deletion of an individual tutor.

    Parameters:
    tutor_id (int): The ID of the tutor to be deleted.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return delete_record_response(Tutor, tutor_id, "Tutor")


# Route to handle the display of all tutors. Access to this route is restricted to logged-in users.
@tutors.route("/tutors", methods=["GET", "POST"])
@login_required
def display_all_tutors():
    """
    This function handles the display of all tutors.

    It specifies a list of CSS classes to be dynamically resized for a matching display

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return display_all_records_response(Tutor, "Tutor")
