# Third-party libraries
from flask import Blueprint
from flask_login import login_required

# Local application modules
from backend.models import Teacher
from backend.forms.teacher_forms import TeacherForm
from backend.responses.common_responses import add_record_response, update_record_response, delete_record_response, \
                                                 display_record_response, display_all_records_response
from backend.utils.common_utils import check_staff_records_match, populate_staff_form

# Initializes a Blueprint object to organize teacher-related routes
teachers = Blueprint('teachers', __name__)


# Route to handle adding new teachers. Access to this route is restricted to logged-in users.
@teachers.route("/teachers/new", methods=["GET", "POST"])
@login_required
def add_teacher():
    """
    This function handles the addition of a new teacher.

    It initializes a form instance to capture teacher details.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = TeacherForm()
    return add_record_response(form, Teacher, "Teacher")


# Route to handle updating existing teachers, identified by their unique IDs. Access to this route is restricted to logged-in users.
@teachers.route("/teachers/<int:teacher_id>/edit", methods=["GET", "POST"])
@login_required
def update_teacher(teacher_id):
    """
    This function handles the updating of an existing teacher.

    Parameters:
    teacher_id (int): The ID of the teacher to be updated.

      It performs the following steps:
        1. Initializes a form instance to capture updated student details.
        2. Changes the default form submission button text to 'Update Teacher'.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = TeacherForm()
    form.set_submit_text("Update Teacher")
    return update_record_response(form, Teacher, populate_staff_form, check_staff_records_match, teacher_id, "Teacher")


# Route to handle the display of individual teachers, identified by their unique IDs. Access to this route is restricted to logged-in users.
@teachers.route("/teachers/<int:teacher_id>")
@login_required
def display_teacher(teacher_id):
    """
    This function handles the display of individual teacher details.

    Parameters:
    teacher_id (int): The ID of the teacher to be displayed.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return display_record_response(Teacher, teacher_id, "Teacher")


# Route to handle the deletion of individual teachers, identified by their unique IDs. Access to this route is restricted to logged-in users.
@teachers.route("/teachers/<int:teacher_id>/delete", methods=["POST"])
@login_required
def delete_teacher(teacher_id):
    """
    This function handles the deletion of an individual teacher.

    Parameters:
    teacher_id (int): The ID of the teacher to be deleted.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return delete_record_response(Teacher, teacher_id, "Teacher")


# Route to handle the display of all teachers. Access to this route is restricted to logged-in users.
@teachers.route("/teachers", methods=["GET", "POST"])
@login_required
def display_all_teachers():
    """
    This function handles the display of all teachers.

    It specifies a list of CSS classes to be dynamically resized for a matching display

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return display_all_records_response(Teacher, "Teacher")
