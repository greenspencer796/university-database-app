# Third-party libraries
from flask import Blueprint
from flask_login import login_required

# Local application modules
from backend.models import Course, Teacher
from backend.forms.course_forms import CourseForm
from backend.responses.common_responses import add_record_response, update_record_response, delete_record_response, \
                                                 display_record_response, display_all_records_response
from backend.utils.common_utils import choices_list_builder
from backend.utils.course_utils import populate_course_form, check_course_records_match

# Initializes a Blueprint object to organize course-related routes
courses = Blueprint('courses', __name__)


# Route to handle adding new courses. Access to this route is restricted to logged-in users.
@courses.route("/courses/new", methods=["GET", "POST"])
@login_required
def add_course():
    """
    This function handles the addition of a new course.

      It performs the following steps:
        1. Initializes a form instance to capture course details.
        2. Populates the teacher choices field of the form from the Teacher database table, allowing users to select a teacher when creating a course.
        3. Computes the current number of teachers in the database to ensure choice lists are managed appropriately.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = CourseForm()
    form.teacher.choices = choices_list_builder(Teacher, "Select Teacher")
    number_of_teachers = len(Teacher.query.all())
    return add_record_response(form, Course, "Course", number_of_teachers=number_of_teachers)


# Route to handle updating existing courses, identified by their unique IDs. Access to this route is restricted to logged-in users.
@courses.route("/courses/<int:course_id>/edit", methods=["GET", "POST"])
@login_required
def update_course(course_id):
    """
    This function handles the updating of an existing course.

    Parameters:
    course_id (int): The ID of the course to be updated.

      It performs the following steps:
        1. Initializes a form instance to capture updated course details.
        2. Changes the default form submission button text to 'Update Course'.
        3. Populates the teacher choices field of the form from the Teacher database table, allowing users to select a teacher when updating a course.
        4. Computes the current number of teachers in the database to ensure choice lists are managed appropriately.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    form = CourseForm()
    form.set_submit_text("Update Course")
    form.teacher.choices = choices_list_builder(Teacher, "Select Teacher")
    number_of_teachers = len(Teacher.query.all())
    return update_record_response(form, Course, populate_course_form, check_course_records_match, course_id, "Course", number_of_teachers)


# Route to handle the display of individual courses, identified by their unique IDs. Access to this route is restricted to logged-in users.
@courses.route("/courses/<int:course_id>")
@login_required
def display_course(course_id):
    """
    This function handles the display of individual course details.

    Parameters:
    course_id (int): The ID of the course to be displayed.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return display_record_response(Course, course_id, "Course")


# Route to handle the deletion of individual courses, identified by their unique IDs. Access to this route is restricted to logged-in users.
@courses.route("/courses/<int:course_id>/delete", methods=["POST"])
@login_required
def delete_course(course_id):
    """
    This function handles the deletion of an individual course.

    Parameters:
    course_id (int): The ID of the course to be deleted.

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    return delete_record_response(Course, course_id, "Course")


# Route to handle the display of all courses. Access to this route is restricted to logged-in users.
@courses.route("/courses", methods=["GET", "POST"])
@login_required
def display_all_courses():
    """
    This function handles the display of all courses.

    It specifies a list of CSS classes to be dynamically resized for a matching display

    Returns:
    function: A function that determines what should be sent back to the user, based on the functions parameters
    """
    elements_to_dynamically_resize = ["record-name", "course-teacher"]
    return display_all_records_response(Course, "Course", elements_to_dynamically_resize)
