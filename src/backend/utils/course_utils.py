def populate_course_form(form, course):
    """
    This function populates the given form with data from a course record.

    Parameters:
    form (FlaskForm): The form that needs to be populated with data.
    course (db.Model instance): The course record containing the data to populate the form with.

      It performs the following steps:
        1. Uses the attributes of the 'course' record to populate the corresponding attributes of the 'form' object.
        2. If the course has a teacher associated with it, the teacher's ID is used to populate the 'teacher' field in the form;
           otherwise, it is set to None.
    """
    form.name.data = course.name
    form.length.data = course.length
    form.course_fee.data = course.course_fee
    form.teacher.process_data(course.teacher.id if course.teacher else None)


def check_course_records_match(course, form):
    """
    This function checks if the data in a form matches the data in a course record.

    Parameters:
    course (db.Model instance): The course record containing the original data.
    form (FlaskForm): The form containing the data to be checked against the course record.
    
    It compares each field in the 'form' with the corresponding field in the 'course' record.

    Returns:
    bool: True if all fields match, indicating that the records are identical,and False otherwise.
    """
    if form.name.data.strip() == course.name and form.length.data == course.length and form.course_fee.data == course.course_fee \
       and (course.teacher and int(form.teacher.data) == course.teacher.id):
        return True
    else:
        return False

