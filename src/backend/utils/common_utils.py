# Local application modules
from backend import db

# Third-party libraries
from flask import make_response
from sqlalchemy.exc import SQLAlchemyError


def choices_list_builder(database_table, message):
    """
    This function constructs a list of choices from records in a database table, which can be used to populate a selection field in a form.

    Parameters:
    database_table (db.Model): The database table model to query records from.
    message (str): A message to be displayed as the default choice in the choices list.

      It performs the following steps:
        1. Queries all records from the specified database table.
        2. Initializes the choices list with a default choice containing the provided message using a tuple with an empty string and the message.
        3. Loops through each record retrieved from the database table and for each, creates a tuple containing the record's ID and name.
        4. Adds the choice tuple to the choices list.

    Returns:
    list: A list of tuples representing the choices, where each tuple consists of a record's ID and name.
    """
    records = database_table.query.all()
    choices = [("", message)]
    for record in records:
        choice = (record.id, record.name)
        choices.append(choice)
    return choices


def send_response(html_content, status_code):
    """
    This function creates an HTTP response object and sets its content and status code as specified by the parameters.

    Parameters:
    html_content (str): The HTML content that will be included in the response body.
    status_code (int): The HTTP status code that will be assigned to the response object.

      It performs the following steps:
        1. Invokes the `make_response` function with the given HTML content to construct a response object.
        2. Sets the status code of this response object to the specified status code.

    Returns:
    response: A response object with the specified HTML content and status code.
    """
    response = make_response(html_content)
    response.status_code = status_code
    return response


def commit_to_db():
    """
    This function attempts to commit changes made in a session to the database.

      It performs the following steps:
        1. Uses the session object's commit method to commit any pending transactions to the database.
        2. If a SQLAlchemyError exception is raised, it prints the error message and rolls back the transaction.

    Returns:
    str: A message indicating the result of the commit operation.
    """
    try:
        db.session.commit()
        return "Committed Successfully"
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        if "Duplicate entry" in str(e):
            return "Unique Error"
        return "General Error"


def check_staff_records_match(staff, form):
    """
    This function checks if the data in a form matches the data in a staff record.
    The staff record can be either a tutor or a teacher record.

    Parameters:
    staff (db.Model instance): The staff record containing the original data.
    form (FlaskForm): The form containing the data to be checked against the staff record

      It performs the following steps:
        1. Compares each field in the 'form' with the corresponding field in the 'staff' record.

    Returns:
    bool: True if all fields match, indicating that the records are identical, and False otherwise.
    """
    if form.name.data.strip() == staff.name and form.age.data == staff.age and form.gender.data == staff.gender.value:
        return True
    else:
        return False


def populate_staff_form(form, staff):
    """
    This function populates a form object with data from a teacher or a tutor record.

    Parameters:
    form (FlaskForm): The form that needs to be populated with data.
    staff (db.Model instance): The teacher or tutor record containing the data to populate the form with.

    It performs the following steps:
        1. Uses the 'staff' record to populate the corresponding attributes of the 'form' object.
        2. The 'gender' field is processed to match the expected format in the form.
    """
    form.name.data = staff.name
    form.age.data = staff.age
    form.gender.process_data(staff.gender.value)
