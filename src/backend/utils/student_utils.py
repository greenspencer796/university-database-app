def populate_student_form(form, student):
    """
    This function populates a form object with data from a student record.

    Parameters:
    form (FlaskForm): The form that needs to be populated with data.
    student (db.Model instance): The student record containing the data to populate the form with.

      It performs the following steps:
        1. Uses the 'student' record to populate the corresponding attributes of the 'form' object.
        2. If the student has a tutor or course associated with them, their IDs are used to populate the respective fields in the form;
           otherwise, they are set to None.
    """
    form.name.data = student.name
    form.age.data = student.age
    form.gender.process_data(student.gender.value)
    form.enrollment_date.data = student.enrollment_date
    form.scholarship.data = student.scholarship
    form.tutor.process_data(student.tutor.id if student.tutor else None)
    form.course.process_data(student.course.id if student.course else None)


def check_student_records_match(student, form):
    """
    This function checks if the data in a form matches the data in a student record.

    Parameters:
    student (db.Model instance): The student record containing the original data.
    form (StudentForm): The form containing the data to be checked against the course record.

    It compares each field in the 'form' with the corresponding field in the 'student' record

    Returns:
    bool: True if all fields match, indicating that the records are identical, and False otherwise.
    """
    if form.name.data.strip() == student.name and form.age.data == student.age and form.gender.data == student.gender.value \
       and form.scholarship.data == student.scholarship and form.enrollment_date.data == student.enrollment_date and \
       (student.tutor and int(form.tutor.data) == student.tutor.id) and (student.course and int(form.course.data) == student.course.id):
        return True
    else:
        return False
