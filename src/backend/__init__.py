# Import necessary modules and classes
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from dotenv import load_dotenv
import os

load_dotenv()

# Initialize the Flask application
app = Flask(__name__, template_folder='../ui_templates')

# Configure the secret key for session management
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")

# Configure the database URI for SQLAlchemy to interact with the SQLite database
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DB_CONNECT_STRING")

# Initialize the database instance
db = SQLAlchemy(app)

# Initialize the Bcrypt instance for password hashing
bcrypt = Bcrypt(app)

# Initialize the login manager instance and set the view and message category for login
login_manager = LoginManager(app)
login_manager.login_view = "authentication.login"
login_manager.login_message_category = "info"

# Import the various routes from different modules in the backend package
from backend.routes.authentication_routes import authentication
from backend.routes.student_routes import students
from backend.routes.teacher_routes import teachers
from backend.routes.tutor_routes import tutors
from backend.routes.course_routes import courses
from backend.routes.main_routes import main

# Register the blueprints for different parts of the application,
# This helps in organizing routes into separate modules
app.register_blueprint(authentication)
app.register_blueprint(students)
app.register_blueprint(teachers)
app.register_blueprint(tutors)
app.register_blueprint(courses)
app.register_blueprint(main)
