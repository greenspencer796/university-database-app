# Third-party libraries
from flask import url_for, flash, redirect, render_template, request

# Local application modules
from backend.utils.common_utils import send_response
from backend.services.common_services import search_table_handler, fetch_record_handler, delete_record_handler, \
                                                create_record_handler, update_record_handler


def add_record_response(form, table, record_type, number_of_tutors=None, number_of_courses=None, number_of_teachers=None):
    """
    This function handles responses during the addition of a new record to the database. It can add various types of records
    including Student, Course, Teacher, or Tutor.

    Parameters:
    form (FlaskForm): The FlaskForm containing the data for the new record.
    table (db.Model): The database table where the new record should be added.
    record_type (str): The type of record to be added, which can be either 'Student', 'Course', 'Teacher', or 'Tutor'.
    number_of_tutors (int, optional): The current number of tutors in the database. Defaults to None.
    number_of_courses (int, optional): The current number of courses in the database. Defaults to None.
    number_of_teachers (int, optional): The current number of teachers in the database. Defaults to None.

      It performs the following steps:
       1. Validates the form data, and if valid, invokes the create_record_handler function with the form, table, and record_type as arguments,
          to try and create a new record in the database.
       2. Checks the result message from the handler function and, based on the message, handles successful or unsuccessful record creation accordingly,
          displaying appropriate flash messages and redirects.
       3. If the form validation fails, it renders a template to allow the user to try adding a record again, providing them with
          relevant information like the number of existing tutors, courses, or teachers.

    Returns:
    Response object: A Flask response object, which either redirects the user to a different page or renders a template with a specific status code,
                     based on the success or failure of the record addition process.
    """
    # Validate the form data and initiate the process to create a new record in the database using the create_record_handler function
    if form.validate_on_submit():

        # Call the create_record_handler function with form data, table, and record_type and retrieve the newly created record and result message
        record, result_message = create_record_handler(form, table, record_type)

        # If the record creation was successful, flash a success message and redirect to the record's display page
        if result_message == f"{record_type} Created":
            flash(result_message, "success")
            return redirect(url_for(f"{record_type.lower()}s.display_{record_type.lower()}",
                                    **{f"{record_type.lower()}_id": record.id}))

        # If the record already exists in the database, flash a danger message and redirect to the add record page to try again
        elif result_message == f"This {record_type.lower()} already exists in the database":
            flash(result_message, "danger")
            return redirect(url_for(f"{record_type.lower()}s.add_{record_type.lower()}"))

        # If there was an error with the database, flash a danger message and redirect to the add record page to try again
        else:
            flash("There has been an error with the database, please try again", "danger")
            return redirect(url_for(f"{record_type.lower()}s.add_{record_type.lower()}"))

    # If the form data is not valid, the function creates an HTTP response object and sets its content and status code as specified by the parameters
    else:
        return send_response(render_template(
            f"add_new/add_new_record.html",
            form=form, title=f"Add {record_type}",
            number_of_tutors=number_of_tutors,
            number_of_courses=number_of_courses,
            number_of_teachers=number_of_teachers),
            200)


def update_record_response(form, table, populate_form_handler, check_records_match, record_id, record_type, number_of_tutors=None,
                           number_of_courses=None, number_of_teachers=None):
    """
    This function handles responses during the updating of a record in the database, which could be Student, Course, Teacher, or Tutor

    Parameters:
    form (FlaskForm): The FlaskForm containing the updated data for the record.
    table (db.Model): The database table where the record exists.
    populate_form_handler (function): The function that populates the form with the existing data of the record.
    check_records_match (function): A function that checks if the existing and updated records are identical.
    record_id (int): The ID of the record to be updated.
    record_type (str): The type of record to be updated, which can be either 'Student', 'Course', 'Teacher', or 'Tutor'.
    number_of_tutors (int, optional): The current number of tutors in the database. Defaults to None.
    number_of_courses (int, optional): The current number of courses in the database. Defaults to None.
    number_of_teachers (int, optional): The current number of teachers in the database. Defaults to None.

      It performs the following steps:
       1. Fetches the existing record based on the record ID from the specified database table. If the record doesn't exist, it returns an 404 error page.
       2. If the request method is GET, it populates the form with the existing data of the record using the provided populate_form_handler function.
       3. Validates the form data and, if valid, invokes the update_record_handler function with the existing record, form data, record type,
          and the check_records_match function to update the record in the database.
       4. Based on the result message returned by the handler function, it handles different scenarios such as no data change, database error,
          successful update, or duplicate record, displaying appropriate flash messages and redirects.

    Returns:
    Response object: A Flask response object, which either redirects the user to a different page or renders a template with a specific status code,
                     based on the success or failure of the record update process.
    """
    # Fetch the existing record based on the record ID, and if not found, return a 404 error page.
    record = fetch_record_handler(table, record_id)
    if not record:
        return send_response(render_template(
            "error/error_page.html",
            status_code=404,
            message=f"Cannot update {record_type.lower()} as an invalid ID was passed into the URL."),
            404)

    # If the request method is GET, populate the form with the existing data of the record using the provided handler function
    if request.method == "GET":
        populate_form_handler(form, record)

    # Validate the form data and initiate the process to update the record in the database using the update_record_handler function
    if form.validate_on_submit():

        # Call the update_record_handler function with the existing record, form data, record type, and check_records_match function, and retrieve the result message
        result_message = update_record_handler(record, form, record_type, check_records_match)

        # If the form matches the existing record, flash an info message and redirect to the update record page to try again
        if result_message == "No Data Changed":
            flash(f"This {record_type.lower()} has not been updated, as no data has been changed", "info")
            return redirect(url_for(f"{record_type.lower()}s.update_{record_type.lower()}", **{f"{record_type.lower()}_id": record.id}))

        # If there was an error with the database, flash a danger message and redirect to the update record page to try again
        elif result_message == "Database Error":
            flash("There has been an error with the database, please try again", "danger")
            return redirect(url_for(f"{record_type.lower()}s.update_{record_type.lower()}", **{f"{record_type.lower()}_id": record.id}))

        # If the record update was successful, flash a success message and redirect to the record's display page
        elif result_message == f"{record_type} Updated":
            flash(result_message, "success")
            return redirect(url_for(f"{record_type.lower()}s.display_{record_type.lower()}", **{f"{record_type.lower()}_id": record.id}))

        # If the record already exists in the database, flash a danger message and redirect to the update record page to try again
        elif result_message == f"This {record_type.lower()} already exists in the database":
            flash(result_message, "danger")
            return redirect(url_for(f"{record_type.lower()}s.update_{record_type.lower()}", **{f"{record_type.lower()}_id": record.id}))

    # If the form data is not valid, the function creates an HTTP response object and sets its content and status code as specified by the parameters
    else:
        return send_response(render_template(
            f"add_new/add_new_record.html",
            record_type=record_type,
            title=f"Update {record_type}",
            form=form,
            record_id=record.id,
            number_of_tutors=number_of_tutors,
            number_of_courses=number_of_courses,
            number_of_teachers=number_of_teachers),
            200)


def display_record_response(table, record_id, record_type):
    """
    This function is responsible for handling responses when displaying a specific record from the database.

    Parameters:
    table (db.Model): The database table where the record is stored.
    record_id (int): The ID of the record to be displayed.
    record_type (str): The type of record to be displayed, which can either be 'Student', 'Course', 'Teacher', or 'Tutor'.

    The function performs the following steps:
      1. Fetches the existing record from the specified database table using the record_id. If the record doesn't exist,
         it displays a 404 error page with an appropriate message.
      2. If the record exists, it sends a response to render a template to display the record details.
         The template path and details displayed are determined based on the record_type.

    Returns:
    Response object: A Flask response object that either renders a template displaying the record details with a 200 status code
                     or a 404 error page if the record doesn't exist.
    """
    # Fetch the existing record from the specified database table using the record_id. If not found, return a 404 error response
    record = fetch_record_handler(table, record_id)
    if not record:
        return send_response(render_template(
            "error/error_page.html",
            status_code=404,
            message=f"Cannot display {record_type.lower()} as an invalid ID was passed into the URL."),
            404)
    else:
        # If the record exists, the function creates an HTTP response object and sets its content and status code as specified by the parameters
        return send_response(render_template(
            f"display/{'staff' if record_type == 'Tutor' or record_type == 'Teacher' else record_type.lower()}.html",
            table_name=table.__tablename__,
            title=record.name,
            record_type=record_type,
            record_id=record.id,
            record=record),
            200)


def delete_record_response(table, record_id, record_type):
    """
    This function handles the responses when an attempt is made to delete a specific record from the database.

    Parameters:
    table (db.Table): The database table from which the record is to be deleted.
    record_id (int): The ID of the record that is intended to be deleted.
    record_type (str): The type of record being dealt with, which can either be 'Student', 'Course', 'Teacher', or 'Tutor'.

      The function performs the following steps:
        1. Invokes the delete_record_handler function with the record_id and table as arguments.
        2. Based on the result message returned by the handler function, it handles different scenarios such as invalid ID, database error,
           or record deleted, displaying appropriate flash messages and redirects.

    Returns:
    Response object: A Flask response object which either renders a template displaying an error message with an appropriate status code,
                     or redirects the user to another page, based on the outcome of the delete operation.
    """
    # Attempt to delete the record by calling the delete_record_handler with the table and record_id as arguments, and storing the result message
    result_message = delete_record_handler(table, record_id)

    # If the record ID was invalid, the function creates an HTTP response object and sets its content and status code as specified by the parameters
    if result_message == "Invalid ID":
        return send_response(render_template(
            "error/error_page.html",
            status_code=404,
            message=f"Cannot delete {record_type.lower()} as an invalid ID was passed into the URL."),
            404)

    # If there was a database error during the delete operation, flash a danger message and redirect to the record's display page
    elif result_message == "Database Error":
        flash("There has been an error with the database. Please try again.", "danger")
        return redirect(url_for(f"{record_type.lower()}s.display_{record_type.lower()}",
                                **{f"{record_type.lower()}_id": record_id}))

    # If the record was successfully deleted, flash a success message and redirect to the page displaying all records of the specified type
    elif result_message == "Record Deleted":
        flash(result_message, "success")
        return redirect(url_for(f"{record_type.lower()}s.display_all_{record_type.lower()}s"))


def display_all_records_response(table, record_type, elements_to_dynamically_resize=['record-name']):
    """
    This function manages the responses for displaying all records of a specific type on a page.

    Parameters:
    table (db.Model): The database table containing the records to be displayed.
    record_type (str): The type of record to be displayed, which can either be 'Student', 'Course', 'Teacher', or 'Tutor'.
    elements_to_dynamically_resize (list): A list of CSS class names for elements that need dynamic resizing on the page. Default is ['record-name'].

      The function performs the following steps:
        1. Retrieves the search term from the POST request, if present, it sanitizes it by stripping whitespace and converting it to lowercase.
        2. Invokes the search_table_handler function with the table and search term as arguments to get the list of records that match the search term (if any).
        3. Renders a template to display all the relevant records (or all records if no search term was provided), passing the necessary data to the template.

    Returns:
    Response object: A Flask response object that renders a template to display all the records (or the search results), with a status code of 200.
    """

    # Retrieve the search term from the POST request if present, otherwise default to None
    search_term = request.form['searchBar'].strip().lower() if request.method == 'POST' else None

    # Call the search_table_handler function to get the records matching the search term and a boolean indicating whether a search has occurred
    searched_records, has_searched = search_table_handler(table, search_term)

    # Return a function that creates an HTTP response object and sets its content and status code as specified by the parameters
    return send_response(render_template(
        f"display_all/{'staff' if record_type == 'Tutor' or record_type == 'Teacher' else record_type.lower() + 's'}.html",
        title=f"{record_type}s",
        record_type=record_type,
        all_searched_records=searched_records,
        length_of_list=len(searched_records),
        has_searched=has_searched,
        search_bar_text=f"Search for a {record_type.lower()}",
        add_record_url=f"{record_type.lower()}s.add_{record_type.lower()}",
        elements_to_dynamically_resize=elements_to_dynamically_resize),
        200)

