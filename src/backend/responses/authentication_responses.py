# Third-party libraries
from flask import render_template, url_for, flash, redirect
from flask_login import current_user

# Local application modules
from backend.utils.common_utils import send_response


def authentication_response(form, authentication_handler, title, successful_auth_redirect_function, authentication_error_message):
    """
    This function handles responses during the authentication processes, such as user registration and login.

    Parameters:
    form (FlaskForm): The FlaskForm containing authentication data, including fields like email and password.
    authentication_handler (function): The handler function that manages user authentication. It either handles the creation
                                       of a new user account or logs in an existing user, depending on the specified title parameter.
    title (str): The title representing the authentication process underway, which can either be "Register" or "Login".
    successful_auth_redirect_function (str): The name of the function to redirect to upon successful authentication.
    authentication_error_message (str): The error message to display upon unsuccessful authentication

      It performs the following steps:
       1. Checks if a user is already authenticated, if so, redirects them to the home page.
       2. Validates the form data, and if valid, invokes the authentication handler function with the form as an argument,
          to try and register or login the user.
       3. Checks the result message from the handler function and, based on the message, handles successful or unsuccessful
          authentication attempts accordingly, displaying appropriate flash messages and redirects.

    Returns:
    Response object: A Flask response object, which either redirects the user to a different page or renders a template with
                     a specific status code, based on the success or failure of the authentication process.
    """
    # Check if the user is already authenticated, if so, redirect them to the home page
    if current_user.is_authenticated:
        return redirect(url_for("main.home"))

    # Validate the form data and handle it using the provided authentication handler function
    if form.validate_on_submit():

        # Call the authentication handler function with the form data and retrieve the result message
        result_message = authentication_handler(form)

        # If the authentication was successful (either account was created or user was logged in), handle redirection and flash messages accordingly
        if result_message == "Account Created" or result_message == "User Logged In":
            if title == "Register":
                flash("Account created, you can now login", "success")
            return redirect(url_for(successful_auth_redirect_function))
        else:
            # If the authentication was unsuccessful, flash the error message and handle redirection accordingly
            flash(authentication_error_message, 'danger')
            if title == "Login":
                return redirect(url_for("authentication.login"))
            else:
                return redirect(url_for("authentication.register"))
    else:
        # If the form data is not valid, the function creates an HTTP response object and sets its content and status code as specified by the parameters
        return send_response(render_template(
            "authentication/authentication.html",
            form=form,
            title=title),
            200)

