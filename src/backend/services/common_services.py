# Local application modules
from backend import db
from backend.utils.common_utils import commit_to_db
from backend.models import Course, Tutor, Teacher


def fetch_record_handler(table, record_id):
    """
    This function handles fetching a record from a database table by its ID.

    Parameters:
    table (db.Model): The database table model to query records from.
    record_id (int): The unique identifier of the record to be fetched.

      It performs the following steps:
        1. Queries the database for the specified record.
        2. Checks if the record exists

    Returns:
    db.Model instance or None: The fetched record object if it exists, otherwise None.
    """
    record = db.session.get(table, record_id)
    if not record:
        return None
    else:
        return record


def delete_record_handler(table, record_id):
    """
    This function attempts to delete a record from a database table identified by the given record ID.

    Parameters:
    table (db.Model): The database table model to delete the record from.
    record_id (int or str): The ID of the record to be deleted.

      It performs the following steps:
        1. Calls the `fetch_record_handler` function to fetch the respective record.
        2. Checks if the fetched record is exists
        3. If the record is found, it proceeds to delete the record from the database session.
        4. Attempts to commit the session to the database, using 'commit_to_db()'
        5. Checks the commit result message

    Returns:
    str: A message indicating the result of the operation.
    """
    record = fetch_record_handler(table, record_id)
    if not record:
        return "Invalid ID"

    db.session.delete(record)

    commit_result_message = commit_to_db()

    if commit_result_message == "Committed Successfully":
        return "Record Deleted"
    else:
        return "Database Error"


def search_table_handler(table, search_term):
    """
    This function searches for records in a database table based on a provided search term. If no search term is provided,
    it returns all records in the table.

    Parameters:
    table (db.Model): The database table model to query the records from.
    search_term (str): The term to search for in each record's name.

      It performs the following steps:
        1. Checks if a search term is provided
        2. If provided, performs a case-insensitive search in the table where the name contains the search term, and retrieves all matching records.
        3. If not provided, retrieves all records from the table.

    Returns:
    tuple: A tuple containing a list of records and a boolean indicating whether a search was performed.
    """
    if search_term:
        records = db.session.query(table).filter(table.name.ilike(f"%{search_term}%")).all()
        return records, True
    else:
        records = db.session.query(table).all()
        return records, False


def create_record_handler(form, table, record_type):
    """
    This function handles the creation of a new record in the database for different types of records: Teacher, Tutor, Student, and Course.

    Parameters:
    form (FlaskForm): The form containing the data for the new record.
    table (db.Model): The database table model where the new record will be added.
    record_type (str): The type of record to be created - "Teacher", "Tutor", "Student", or "Course".

      It performs the following steps:
        1. Evaluates the record_type parameter to determine the structure of the new record.
        2. Depending on the record_type, it may perform additional fetch operations to retrieve associated course, teacher, or tutor records.
        3. Creates a new record instance with data from the form.
        4. Adds the new record to the database session.
        5. Attempts to commit the session to the database using the commit_to_db function.
        6. Evaluates the commit result message to determine the outcome of the operation.


    Returns:
    tuple: A tuple containing the newly created record object (or None if an error occurred) and a message indicating the outcome
           of the operation.
    """
    if record_type == "Teacher" or record_type == "Tutor":
        record = table(name=form.name.data.strip(), age=form.age.data, gender=form.gender.data)
    elif record_type == "Student":
        course = db.session.get(Course, form.course.data)
        tutor = db.session.get(Tutor, form.tutor.data)
        record = table(name=form.name.data.strip(), age=form.age.data, gender=form.gender.data, enrollment_date=form.enrollment_date.data, scholarship=form.scholarship.data, tutor=tutor, course=course)
    elif record_type == "Course":
        teacher = db.session.get(Teacher, form.teacher.data)
        record = table(name=form.name.data.strip(), length=form.length.data, course_fee=form.course_fee.data, teacher=teacher)

    db.session.add(record)

    commit_result_message = commit_to_db()

    if commit_result_message == "Committed Successfully":
        return record, f"{record_type} Created"
    elif commit_result_message == "Unique Error":
        return None, f"This {record_type.lower()} already exists in the database"
    else:
        return None, "Database Error"


def update_record_handler(record, form, record_type, check_records_match):
    """
    This function handles the updating of an existing record in the database based on the provided record type: "Teacher", "Tutor", "Student", or "Course".

    Parameters:
    record (db.Model instance): The record object to be updated.
    form (FlaskForm): The form containing the new data for the record.
    record_type (str): The type of record to be updated - "Teacher", "Tutor", "Student", or "Course".
    check_records_match (function): A function to check if the data in the form matches the current data in the record.

      It performs the following steps:
        1. Calls `check_records_match` with the record and form as arguments to check if any data has changed.
        2. If the data has changed it proceeds to update the record with new data:
        3. If the record_type is "Student" or "Course", additional fetches are sent to get the record's associated course, teacher or tutor.
        4. Attempts to commit the session to the database, using 'commit_to_db()'.
        5. Evaluates the commit result message to determine the outcome

    Returns:
    str: A message indicating the result of the operation.
    """
    if check_records_match(record, form):
        return "No Data Changed"

    if record_type == "Teacher" or record_type == "Tutor":
        record.name = form.name.data.strip()
        record.age = form.age.data
        record.gender = form.gender.data

    elif record_type == "Student":
        course = db.session.get(Course, form.course.data)
        tutor = db.session.get(Tutor, form.tutor.data)
        record.name = form.name.data.strip()
        record.age = form.age.data
        record.gender = form.gender.data
        record.enrollment_date = form.enrollment_date.data
        record.scholarship = form.scholarship.data
        record.tutor = tutor
        record.course = course

    elif record_type == "Course":
        teacher = db.session.get(Teacher, form.teacher.data)
        record.name = form.name.data.strip()
        record.length = form.length.data
        record.course_fee = form.course_fee.data
        record.teacher = teacher

    commit_result_message = commit_to_db()

    if commit_result_message == "Committed Successfully":
        return f"{record_type} Updated"
    elif commit_result_message == "Unique Error":
        return f"This {record_type.lower()} already exists in the database"
    else:
        return "Database Error"
