# Third-party libraries
from flask_login import login_user

# Local application modules
from backend import bcrypt, db
from backend.models import User
from backend.utils.common_utils import commit_to_db


def create_user_handler(form):
    """
    This function handles the process of creating a new user.

    Parameters:
    form (FlaskForm): The Flask Form containing the data needed to create a new user.

      It performs the following steps:
        1. Hashes the password inputted from the form using bcrypt to enhance security, decoding the hash to a UTF-8 string afterwards.
        2. Creates a new User instance, initializing it with the data from the form, and the hashed password.
        3. Adds the new User instance to the current database session.
        4. Attempts to commit the session to the database, using 'commit_to_db()'

    Returns:
    str: 'Account Created' if the user data is successfully committed to the database, or 'Database Error' if the commit operation fails.
    """
    hashed_password = bcrypt.generate_password_hash(form.password.data).decode("utf-8")
    user = User(email=form.email.data, password=hashed_password, admin=form.admin.data)
    db.session.add(user)
    if commit_to_db():
        return "Account Created"
    else:
        return "Database Error"


def login_user_handler(form):
    """
    This function handles the process of logging in a user.
    
    Parameters:
    form (FlaskForm): An form containing the data needed to create a new user.

      It performs the following steps:
        1. Queries the database to retrieve a User record matching the email provided in the form.
        2. Verifies if the retrieved User exists and if the hashed password stored in the database matches the password provided in the form.
        3. If the verification is successful, logs the user into the application using Flask's login_user function.

    Returns:
    str: 'User Logged In' if the email and password match a record in the database, or 'Login Unsuccessful' if the match
         is unsuccessful or the user doesn't exist in the database.
    """
    user = User.query.filter_by(email=form.email.data).first()
    if user and bcrypt.check_password_hash(user.password, form.password.data):
        login_user(user)
        return "User Logged In"
    else:
        return "Login Unsuccessful"
