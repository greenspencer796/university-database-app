from backend import db, login_manager
from flask_login import UserMixin
from enum import Enum
from sqlalchemy import CheckConstraint, UniqueConstraint, and_
from datetime import datetime, timedelta


# This decorator registers the function as the user loader callback for Flask-Login.
# Flask-Login uses this callback to reload the user object from the user ID stored in the session.
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


# Defines a User class that inherits from db.Model, representing the user table in the database.
class User(db.Model, UserMixin):
    # Primary key column: A unique identifier for each user record.
    id = db.Column(db.Integer, primary_key=True)

    # Email column: It cannot be null, must be unique across all user records and has a maximum character limit of 100.
    email = db.Column(db.String(100), unique=True, nullable=False)

    # Password column: It cannot be null, and has a maximum character limit of 100.
    password = db.Column(db.String(100), nullable=False)

    # Admin column: It is a boolean value indicating whether the user has admin privileges or not, and it cannot be null.
    admin = db.Column(db.Boolean, nullable=False)

    # Method to represent the User object as a string.
    def __repr__(self):
        return f"User('{self.id}', '{self.email}', '{self.password}', '{self.admin}')"


# GenderEnum class to define a set of possible values for the gender column.
class GenderEnum(Enum):
    male = "Male"
    female = "Female"
    non_binary = "Non-Binary"
    other = "Other"


# Defines a Student class that inherits from db.Model, representing the student table in the database.
class Student(db.Model):
    # Primary key column: A unique identifier for each student record.
    id = db.Column(db.Integer, primary_key=True)

    # Name column: It cannot be null, and has a maximum character limit of 70.
    name = db.Column(db.String(70), nullable=False)

    # Age column: It must be a non-null integer value.
    age = db.Column(db.Integer, nullable=False)

    # Gender column: It uses the GenderEnum class to restrict the values to a predefined set.
    gender = db.Column(db.Enum(GenderEnum, values_callable=lambda obj: [e.value for e in obj]), nullable=False)

    # Enrollment date column: It must be a non-null date value.
    enrollment_date = db.Column(db.Date, nullable=False)

    # Scholarship column: It must be a non-null integer value.
    scholarship = db.Column(db.Boolean, nullable=False)

    # Tutor ID column: A foreign key that references the ID of the tutor assigned to the student.
    tutor_id = db.Column(db.Integer, db.ForeignKey("tutor.id"))

    # Course ID column: A foreign key that references the ID of the course the student is enrolled in.
    course_id = db.Column(db.Integer, db.ForeignKey("course.id"))

    # Table arguments: These define additional constraints on the table
    __table_args__ = (
        # Check constraint to ensure the age is between 15 and 90
        CheckConstraint(and_(age >= 16, age <= 90), name='check_student_age_range'),

        # Check constraint to ensure the enrollment date is not more than 365 days in the future
        CheckConstraint(enrollment_date < (datetime.today() + timedelta(days=365)).date(), name='check_future_date_range'),

        # Check constraint to ensure the enrollment date is not before January 1, 1950
        CheckConstraint(enrollment_date >= datetime(1950, 1, 1).date(), name='check_past_date_range'),

        # Unique constraint to prevent duplicate entries with the same values for these columns
        UniqueConstraint('name', 'age', 'gender', 'enrollment_date', 'scholarship', 'tutor_id', 'course_id', name='unique_student_record')
    )

    # Method to represent the Student object as a string.
    def __repr__(self):
        return f"Student('{self.id}', '{self.name}', '{self.age}', '{self.gender}', '{self.enrollment_date}', '{self.scholarship}', '{self.tutor_id}', '{self.course_id}')"


# Defines a Tutor class that inherits from db.Model, representing the tutor table in the database.
class Tutor(db.Model):
    # Primary key column: A unique identifier for each tutor record.
    id = db.Column(db.Integer, primary_key=True)

    # Name column: It cannot be null, and has a maximum character limit of 70.
    name = db.Column(db.String(70), nullable=False)

    # Age column: It must be a non-null integer value.
    age = db.Column(db.Integer, nullable=False)

    # Gender column: It uses the GenderEnum class to restrict the values to a predefined set.
    gender = db.Column(db.Enum(GenderEnum, values_callable=lambda obj: [e.value for e in obj]), nullable=False)

    # Students relationship: Establishes a one-to-many relationship with the Student class, where one tutor can be associated with many students.
    # The backref attribute creates a backward reference from Student to Tutor, enabling access to the Tutor object from the Student object.
    students = db.relationship("Student", backref="tutor", lazy=True)

    # Table arguments: These define additional constraints on the table
    __table_args__ = (
        # Check constraint to ensure the age is between 18 and 90
        CheckConstraint(and_(age >= 18, age <= 90), name='check_tutor_age_range'),

        # Unique constraint to prevent duplicate entries with the same values for these columns
        UniqueConstraint('name', 'age', 'gender', name='unique_tutor_record')
    )

    # Method to represent the Tutor object as a string.
    def __repr__(self):
        return f"Tutor('{self.id}', '{self.name}', '{self.age}', '{self.gender}')"


# Defines a Course class that inherits from db.Model, representing the course table in the database.
class Course(db.Model):
    # Primary key column: A unique identifier for each course record.
    id = db.Column(db.Integer, primary_key=True)

    # Name column: It cannot be null, and has a maximum character limit of 150.
    name = db.Column(db.String(150), nullable=False)

    # Length column: It must be a non-null integer value indicating the duration of the course.
    length = db.Column(db.Integer, nullable=False)

    # Course fee column: It must be a non-null numeric value with a default value of 9100.00.
    # It specifies the precision and scale of the numeric column, which determines the maximum number of digits
    # and the number of digits to the right of the decimal point, respectively.
    course_fee = db.Column(db.Numeric(precision=10, scale=2), nullable=False, default=9100.00)

    # Teacher ID column: A foreign key that references the ID of the teacher assigned to the course.
    teacher_id = db.Column(db.Integer, db.ForeignKey("teacher.id"))

    # Students relationship: Establishes a one-to-many relationship with the Student class, where one course can be associated with many students.
    # The backref attribute creates a backward reference from Student to Course, enabling access to the Course object from the Student object.
    students = db.relationship("Student", backref="course", lazy=True)

    # Table arguments: These define additional constraints on the table
    __table_args__ = (
        # Check constraint to ensure the length of the course is between 1 and 10
        CheckConstraint(and_(length >= 1, length <= 10), name='check_length_range'),

        # Check constraint to ensure the course fee is between 5000 and 25000
        CheckConstraint(and_(course_fee >= 5000, course_fee <= 25000), name='check_fee_range'),

        # Unique constraint to prevent duplicate entries with the same values for these columns
        UniqueConstraint('name', 'length', 'course_fee', 'teacher_id', name='unique_course_record')
    )


# Defines a Teacher class that inherits from db.Model, representing the course table in the database.
class Teacher(db.Model):
    # Primary key column: A unique identifier for each teacher record.
    id = db.Column(db.Integer, primary_key=True)

    # Name column: It cannot be null, and has a maximum character limit of 70.
    name = db.Column(db.String(70), nullable=False)

    # Age column: It must be a non-null integer value.
    age = db.Column(db.Integer, nullable=False)

    # Gender column: It uses the GenderEnum class to restrict the values to a predefined set.
    gender = db.Column(db.Enum(GenderEnum, values_callable=lambda obj: [e.value for e in obj]), nullable=False)

    # Course relationship: Establishes a one-to-many relationship with the Course class, where one teacher can be associated with many courses.
    # The backref attribute creates a backward reference from Course to Teacher, enabling access to the Teacher object from the Course object.
    course = db.relationship("Course", backref="teacher", lazy=True)

    # Table arguments: These define additional constraints on the table
    __table_args__ = (
        # Check constraint to ensure the age is between 18 and 90.
        CheckConstraint(and_(age >= 18, age <= 90), name='check_teacher_age_range'),

        # Unique constraint to prevent duplicate entries with the same values for these columns.
        UniqueConstraint('name', 'age', 'gender', name='unique_teacher_record')
    )

    # Method to represent the Teacher object as a string.
    def __repr__(self):
        return f"Teacher('{self.id}', '{self.name}', '{self.age}', '{self.gender}')"

