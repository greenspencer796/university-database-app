from enum import Enum

def compare_records(record1, record2, attributes):
    """
    This function compares two records to check if they are identical in terms of specified attributes.

    Parameters:
    record1 (db.Model instance): The first record to be compared.
    record2 (db.Model instance): The second record to be compared.
    attributes (list): A list of the attributes (column names) that need to be compared between the two records.

    The function performs the following steps:
        1. Iterates through each attribute in the attributes list.
        2. For each attribute, it retrieves the value from both records using the getattr function.
        3. Compares the values of these attributes between the two records.

    Returns:
    bool: True if all the specified attributes are identical between the two records; False otherwise.
    """
    for attr in attributes:
        if getattr(record1, attr) != getattr(record2, attr):
            return False
    return True

# GenderEnum class to define a set of possible values for the gender field.
class GenderEnum(Enum):
    male = "Male"
    female = "Female"
    non_binary = "Non-Binary"
    other = "Other"