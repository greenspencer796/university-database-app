# Python libraries
import unittest
from unittest import mock

# Third-party libraries
from sqlalchemy.exc import SQLAlchemyError

# Local application modules
from backend import app, db
from backend.services.common_services import fetch_record_handler, delete_record_handler, search_table_handler
from backend.models import Tutor


class TestFetchRecordHandler(unittest.TestCase):
    tutor = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and setting up a tutor instance which will be used in multiple test cases to avoid redundancy.
        with app.app_context():
            cls.tutor = Tutor(name="Samuel McSmith", age=42, gender="Male")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get' method of the 'db.session' object in the 'backend' module with a mock object.
    @mock.patch('backend.models.db.session.get')
    def test_1_fetch_record_handler_returns_success(self, mock_get):
        with app.app_context():
            # Configuring mock_get to return a predefined tutor record instead of performing its usual function. 
            mock_get.return_value = self.tutor

            # Asserting that the fetch record handler function when successful returns a record
            self.assertEqual(fetch_record_handler(Tutor, self.tutor.id), self.tutor)

            # Asserting that db.session.get() was called exactly once.
            mock_get.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get' method of the 'db.session' object in the 'backend' module with a mock object.
    @mock.patch('backend.models.db.session.get')
    def test_2_fetch_record_handler_returns_none(self, mock_get):
        with app.app_context():
            # Configuring mock_get to return None instead of performing its usual function. 
            mock_get.return_value = None

            # Asserting that the fetch record handler function when unsuccessful returns None
            self.assertEqual(fetch_record_handler(Tutor, 100), None)

            # Asserting that db.session.get() was called exactly once.
            mock_get.assert_called_once()


class TestSearchTableHandler(unittest.TestCase):
    tutor = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and setting up a tutor instance which will be used in multiple test cases to avoid redundancy.
        with app.app_context():
            cls.tutor = Tutor(name="Samuel McSmith", age=42, gender="Male")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'query' method of the 'db.session' object in the 'backend' module with a mock object.
    @mock.patch('backend.models.db.session.query')
    def test_1_search_handler_successful_search(self, mock_query):
        with app.app_context():
            # Configuring mock_query.filter.all to return a predefined tutor record instead of performing its usual function. 
            mock_query.return_value.filter.return_value.all.return_value = self.tutor

            # Asserting that when the search table handler function receives a search term it returns the records that match 
            # the search term and also returns True
            self.assertEqual(search_table_handler(Tutor, self.tutor.name), (self.tutor, True))

            # Asserting that db.session.query was called exactly once.
            mock_query.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'query' method of the 'db.session' object in the 'backend' module with a mock object.
    @mock.patch('backend.models.db.session.query')
    def test_2_search_handler_no_search_term(self, mock_query):
        with app.app_context():
            # Configuring mock_query.all to return a predefined tutor record instead of performing its usual function. 
            mock_query.return_value.all.return_value = self.tutor

            # Asserting that when the search table handler function doesn't receive a search term it returns all the records 
            # in the table and also returns False
            self.assertEqual(search_table_handler(Tutor, None), (self.tutor, False))

            # Asserting that db.session.query was called exactly once.
            mock_query.assert_called_once()


class TestDeleteRecordHandler(unittest.TestCase):
    tutor = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and setting up a tutor instance which will be used in multiple test cases to avoid redundancy.
        with app.app_context():
            cls.tutor = Tutor(name="Samuel McSmith", age=42, gender="Male")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get' method of the 'db.session' object in the 'backend' module with a mock object.
    @mock.patch('backend.models.db.session.get')
    def test_1_delete_record_handler_returns_invalid_id(self, mock_get):
        with app.app_context():
            # Configuring mock_get to return None instead of performing its usual function. 
            mock_get.return_value = None

            # Asserting that when attempting to delete a record with an invalid id the message "Invalid ID" is returned
            self.assertEqual(delete_record_handler(Tutor, 100), "Invalid ID")

            # Asserting that db.session.get() was called exactly once.
            mock_get.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'delete' and 'commit' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.delete')
    @mock.patch('backend.models.db.session.commit')
    def test_2_delete_record_handler_returns_record_deleted(self, mock_commit, mock_delete, mock_get):
        with app.app_context():
            # Configuring mock_get to return a predefined tutor record instead of performing its usual function. 
            mock_get.return_value = self.tutor

            # Asserting that when a record is successfully deleted the message "Record Deleted" is returned
            self.assertEqual(delete_record_handler(Tutor, self.tutor.id), "Record Deleted")

            # Asserting that db.session.get(), db.session.delete() and db.session.commit() were all called exactly once.
            mock_get.assert_called_once()
            mock_delete.assert_called_once()
            mock_commit.assert_called_once()
    
    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'delete', 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.delete')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback') 
    def test_3_delete_record_handler_returns_database_error(self, mock_rollback, mock_commit, mock_delete, mock_get):
        with app.app_context():
            # Configuring mock_get to return a predefined tutor instead of performing its usual function. 
            mock_get.return_value = self.tutor

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Asserting that when attempting to delete a record and a database error occurs "Database Error" is returned
            self.assertEqual(delete_record_handler(Tutor, self.tutor.id), "Database Error")

            # Asserting that db.session.get(), db.session.delete(), db.session.commit() and db.session.rollback() were all called exactly once.
            mock_get.assert_called_once()
            mock_delete.assert_called_once()
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()