# Python libraries
import unittest
from datetime import datetime
from unittest import mock

# Third-party libraries
from sqlalchemy.exc import SQLAlchemyError

# Local application modules
from backend import app, db
from backend.models import Student, Tutor, Teacher, Course
from backend.services.common_services import create_record_handler, update_record_handler
from backend.forms.student_forms import StudentForm
from backend.utils.student_utils import check_student_records_match
from tests.utils import compare_records, GenderEnum


class TestCreateStudentHandler(unittest.TestCase):
    tutor = None
    course = None
    teacher = None
    form = None
    app_context = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and setting up teacher, course and tutor records along with a form instance with preset values. 
        # These records and the form will be used in multiple test cases to avoid redundancy.
        cls.app_context = app.test_request_context()
        with cls.app_context:
            cls.teacher = Teacher(name="Sammy McOlds", age=34, gender="Male")
            cls.teacher.id = 1

            cls.course = Course(name="Modern Fantasy Literature", length=3, course_fee=9100, teacher=cls.teacher)
            cls.course.id = 1

            cls.tutor = Tutor(name="Sarah Jane", age=56, gender="Female")
            cls.tutor.id = 1

            cls.form = StudentForm()
            cls.form.name.data = "Hannah McBana"
            cls.form.age.data = 25
            cls.form.gender.data = "Female"
            cls.form.enrollment_date.data = datetime.strptime('2023-09-15', '%Y-%m-%d').date()
            cls.form.scholarship.data = True
            cls.form.tutor.data = cls.tutor.id
            cls.form.course.data = cls.course.id

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit' and 'add' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    def test_1_create_record_handler_returns_student_created(self, mock_add, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined course record the first time it's called 
            # and predefined tutor record the second time it's called, instead of performing its usual function. 
            mock_get.side_effect = [self.course, self.tutor] 

            # Creating a new student record through the create_record_handler function.
            created_student, result_message = create_record_handler(self.form, Student, "Student")
           
            # Creating an expected student record for comparison.
            expected_student = Student(name=self.form.name.data.strip(), age=self.form.age.data, gender=self.form.gender.data, enrollment_date=self.form.enrollment_date.data, 
                                       scholarship=self.form.scholarship.data, tutor=self.tutor, course=self.course)
            
            # Asserting that db.session.commit() and db.session.add() were both called exactly once
            mock_commit.assert_called_once()
            mock_add.assert_called_once()

            # Asserting that db.session.get() was called exactly twice
            self.assertEqual(mock_get.call_count, 2)

            # Comparing the created student record with the expected student record.
            # This is done by comparing each column from the student table.
            records_match = compare_records(created_student, expected_student, ['name', 'age', 'gender', 'enrollment_date', 'scholarship', 'tutor', 'course'])

            
            # Asserting that the created student matches with the expected student  
            # and checking that the result message indicates successful student creation.
            self.assertEqual(records_match, True)
            self.assertEqual((result_message), "Student Created")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit', 'add' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.rollback')
    def test_2_create_record_handler_returns_student_already_exists(self, mock_rollback, mock_add, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined course record the first time it's called 
            # and predefined tutor record the second time it's called, instead of performing its usual function. 
            mock_get.side_effect = [self.course, self.tutor] 

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "Duplicate entry"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("Duplicate entry")

            # Creating a new student record through the create_record_handler function
            created_student, result_message = create_record_handler(self.form, Student, "Student")

            # Asserting that db.session.commit(), db.session.add() and db.session.rollback() were all called exactly once
            mock_commit.assert_called_once()
            mock_add.assert_called_once()
            mock_rollback.assert_called_once()

            # Asserting that db.session.get() was called exactly twice
            self.assertEqual(mock_get.call_count, 2)

            # Asserting that when attempting to create the student a duplicate error is thrown and the function
            # returns None and a message indicating the student already exists in the database.        
            self.assertEqual((created_student, result_message), (None, "This student already exists in the database"))


    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit', 'add' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.rollback')
    def test_3_create_record_handler_returns_database_error(self, mock_rollback, mock_add, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined course record the first time it's called 
            # and predefined tutor record the second time it's called, instead of performing its usual function. 
            mock_get.side_effect = [self.course, self.tutor] 

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Creating a new student record through the create_record_handler function
            created_student, result_message = create_record_handler(self.form, Student, "Student")

            # Asserting that db.session.commit(), db.session.add() and db.session.rollback() were all called exactly once
            mock_commit.assert_called_once()
            mock_add.assert_called_once()
            mock_rollback.assert_called_once()

            # Asserting that db.session.get() was called exactly twice
            self.assertEqual(mock_get.call_count, 2)

            # Asserting that when attempting to create the student the function will return None and a "Database Error" message,
            # indicating that the record could not be created due to a database error.         
            self.assertEqual((created_student, result_message), (None, "Database Error"))


class TestUpdateStudentHandler(unittest.TestCase):
    tutor = None
    course = None
    teacher = None
    app_context = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and creating teacher, course and tutor records for use in test cases.
        cls.app_context = app.test_request_context()
        with cls.app_context:
            cls.teacher = Teacher(name="Sammy McOlds", age=34, gender="Male")
            cls.teacher.id = 1

            cls.course = Course(name="Modern Fantasy Literature", length=3, course_fee=9100, teacher=cls.teacher)
            cls.course.id = 1

            cls.tutor = Tutor(name="Sarah Jane", age=56, gender="Female")
            cls.tutor.id = 1

    def setUp(self):
        # Setting up the necessary prerequisites for each test case by creating a fresh student record and a form.
        with self.app_context:
            self.form = StudentForm()
            self.form.name.data = "Hannah McBana"
            self.form.age.data = 25
            self.form.gender.data = "Female"
            self.form.enrollment_date.data = datetime.strptime('2023-09-15', '%Y-%m-%d').date()
            self.form.scholarship.data = True
            self.form.tutor.data = self.tutor.id
            self.form.course.data = self.course.id

            self.created_student = Student(name=self.form.name.data.strip(), age=self.form.age.data, gender=GenderEnum(self.form.gender.data), enrollment_date=self.form.enrollment_date.data, 
                                           scholarship=self.form.scholarship.data, tutor=self.tutor, course=self.course)

    def test_1_update_record_handler_returns_no_data_changed(self):
        with self.app_context:
            # Asserting that the function correctly identifies when no changes have been made
            # to the data and returns a message indicating "No Data Changed".
            self.assertEqual(update_record_handler(self.created_student, self.form, "Student", check_student_records_match), "No Data Changed")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get' and 'commit' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    def test_2_update_record_handler_returns_student_updated(self, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined course record the first time it's called 
            # and predefined tutor record the second time it's called, instead of performing its usual function. 
            mock_get.side_effect = [self.course, self.tutor] 

            # Modifying the age data in the form and asserting that the update function identifies
            # the change and returns a message indicating the course has been successfully updated. 
            self.form.age.data = 26
            self.assertEqual(update_record_handler(self.created_student, self.form, "Student", check_student_records_match), "Student Updated")
            
            # Asserting that db.session.commit() was called exactly once.
            mock_commit.assert_called_once()

            # Asserting that db.session.get() was called exactly twice
            self.assertEqual(mock_get.call_count, 2)
            
    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback')
    def test_3_update_record_handler_returns_student_already_exists(self, mock_rollback, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined course record the first time it's called 
            # and predefined tutor record the second time it's called, instead of performing its usual function. 
            mock_get.side_effect = [self.course, self.tutor]

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "Duplicate entry"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("Duplicate entry")

            # Adjusting the form data and asserting that when attempting to update the student a duplicate error is thrown 
            # and the function returns a message indicating the student already exists in the database. 
            self.form.age.data = 27
            self.assertEqual(update_record_handler(self.created_student, self.form, "Student", check_student_records_match), "This student already exists in the database")

            # Asserting that  db.session.commit() and db.session.rollback() were both called exactly once.
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()

            # Asserting that db.session.get() was called exactly twice
            self.assertEqual(mock_get.call_count, 2)

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback')
    def test_4_update_record_handler_returns_database_error(self, mock_rollback, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined course record the first time it's called 
            # and predefined tutor record the second time it's called, instead of performing its usual function. 
            mock_get.side_effect = [self.course, self.tutor]

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Setting the age field to None and asserting that when attempting to update the student 
            # a database error is thrown and the function returns a message indicating there has been an error in the database.
            self.form.age.data = None
            self.assertEqual(update_record_handler(self.created_student, self.form, "Student", check_student_records_match), "Database Error")

            # Asserting that db.session.commit() and db.session.rollback() were both called exactly once.
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()

            # Asserting that db.session.get() was called exactly twice
            self.assertEqual(mock_get.call_count, 2)
