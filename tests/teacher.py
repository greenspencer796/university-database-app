# Python libraries
import unittest
from unittest import mock

# Third-party libraries
from sqlalchemy.exc import SQLAlchemyError

# Local application modules
from backend import app, db
from backend.models import Teacher
from backend.services.common_services import create_record_handler, update_record_handler
from backend.utils.common_utils import check_staff_records_match
from backend.forms.teacher_forms import TeacherForm
from tests.utils import compare_records, GenderEnum


class TestCreateTeacherHandler(unittest.TestCase):
    form = None
    app_context = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and setting up a form instance with preset values.
        # This form instance will be used in multiple test cases to avoid redundancy.
        cls.app_context = app.test_request_context()
        with cls.app_context:
            cls.form = TeacherForm()
            cls.form.name.data = "Kenny McSmith"
            cls.form.age.data = 39
            cls.form.gender.process_data("Male")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'commit' and 'add' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.commit')
    def test_1_create_record_handler_returns_teacher_created(self, mock_commit, mock_add):
        with self.app_context:
            # Creating a new teacher record through the create_record_handler function
            created_teacher, result_message = create_record_handler(self.form, Teacher, "Teacher")

            # Creating an expected teacher record for comparison.
            expected_teacher = Teacher(name=self.form.name.data.strip(), age=self.form.age.data, gender=self.form.gender.data)

            # Asserting that db.session.commit() and db.session.add() were both called exactly once.
            mock_commit.assert_called_once()
            mock_add.assert_called_once()

            # Comparing the created teacher record with the expected teacher record.
            # This is done by comparing each column from the teacher table.
            records_match = compare_records(created_teacher, expected_teacher, ['name', 'age', 'gender'])

            # Asserting that the created teacher matches with the expected teacher
            # and checking that the result message indicates successful teacher creation.
            self.assertEqual(records_match, True)
            self.assertEqual((result_message), "Teacher Created")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'commit', 'add' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.rollback')
    def test_2_create_record_handler_returns_teacher_already_exists(self, mock_rollback, mock_add, mock_commit):
        with self.app_context:
            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "Duplicate entry"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("Duplicate entry")
            
            # Creating a new teacher record through the create_record_handler function
            created_teacher, result_message = create_record_handler(self.form, Teacher, "Teacher")

            # Asserting that db.session.commit(), db.session.add() and db.session.rollback() were all called exactly once.
            mock_commit.assert_called_once()
            mock_add.assert_called_once()
            mock_rollback.assert_called_once()

            # Asserting that when attempting to create the teacher a duplicate error is thrown and the function
            # returns None and a message indicating the teacher already exists in the database.            
            self.assertEqual((created_teacher, result_message), (None, "This teacher already exists in the database"))

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'commit', 'add' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.rollback')
    def test_3_create_record_handler_returns_database_error(self, mock_rollback, mock_add, mock_commit):
        with self.app_context:
            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Creating a new teacher record, with an incomplete form, through the create_record_handler function
            created_teacher, result_message = create_record_handler(self.form, Teacher, "Teacher")

            # Asserting that db.session.commit(), db.session.add() and db.session.rollback() were all called exactly once.
            mock_commit.assert_called_once()
            mock_add.assert_called_once()
            mock_rollback.assert_called_once()

            # Asserting that when attempting to create the teacher the function will return None  and a "Database Error" message,
            # indicating that the record could not be created due to a database error.
            self.assertEqual((created_teacher, result_message), (None, "Database Error"))


class TestUpdateTeacherHandler(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Initializing the app context to interact with the database.
        cls.app_context = app.test_request_context()

    def setUp(self):
        # Setting up the necessary prerequisites for each test case by creating a fresh teacher record.
        with self.app_context:
            self.form = TeacherForm()
            self.form.name.data = "Kenny McSmith"
            self.form.age.data = 39
            self.form.gender.process_data("Male")
            self.created_teacher = Teacher(name=self.form.name.data.strip(), age=self.form.age.data, gender=GenderEnum(self.form.gender.data))

    def test_1_update_record_handler_returns_no_data_changed(self):
        with self.app_context:
            # Asserting that the function correctly identifies when no changes have been made
            # to the data and returns a message indicating "No Data Changed".
            self.assertEqual(update_record_handler(self.created_teacher, self.form, "Teacher", check_staff_records_match), "No Data Changed")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'commit' method of the 'db.session' object in the 'backend' module with a mock object.
    @mock.patch('backend.models.db.session.commit')
    def test_2_update_record_handler_returns_teacher_updated(self, mock_commit):
        with self.app_context:
            # Modifying the age data in the form and asserting that the update function identifies
            # the change and returns a message indicating the teacher has been successfully updated.
            self.form.age.data = 40
            self.assertEqual(update_record_handler(self.created_teacher, self.form, "Teacher", check_staff_records_match), "Teacher Updated")
            
            # Asserting that db.session.commit() was called exactly once.
            mock_commit.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback')
    def test_3_update_record_handler_returns_teacher_already_exists(self, mock_rollback, mock_commit):
        with self.app_context:
            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "Duplicate entry"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("Duplicate entry")

            # Adjusting the form data and asserting that when attempting to update the teacher a duplicate error is thrown 
            # and the function returns a message indicating the teacher already exists in the database. 
            self.form.age.data = 37
            self.assertEqual(update_record_handler(self.created_teacher, self.form, "Teacher", check_staff_records_match), "This teacher already exists in the database")

            # Asserting that db.session.commit() and db.session.rollback() were both called exactly once.
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback')
    def test_4_update_record_handler_returns_database_error(self, mock_rollback, mock_commit):
        with self.app_context:
            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Setting the age field to None and asserting that when attempting to update the teacher 
            # a database error is thrown and the function returns a message indicating there has been an error in the database.
            self.form.age.data = None
            self.assertEqual(update_record_handler(self.created_teacher, self.form, "Teacher", check_staff_records_match), "Database Error")

            # Asserting that db.session.commit() and db.session.rollback() were both called exactly once.
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()