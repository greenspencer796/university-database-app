# Python libraries
import unittest
from unittest import mock

# Third-party libraries
from sqlalchemy.exc import SQLAlchemyError

# Local application modules
from backend import app, db
from backend.models import Course, Teacher
from backend.services.common_services import create_record_handler, update_record_handler
from backend.utils.course_utils import check_course_records_match
from backend.forms.course_forms import CourseForm
from tests.utils import compare_records


class TestCreateCourseHandler(unittest.TestCase):
    teacher = None
    form = None
    app_context = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and setting up a teacher record along with a form instance with preset values. 
        # This record and form will be used in multiple test cases to avoid redundancy.
        cls.app_context = app.test_request_context()
        with cls.app_context:
            cls.teacher = Teacher(name="Olly McGuiness", age=47, gender="Male")
            cls.teacher.id = 1

            cls.form = CourseForm()
            cls.form.name.data = "Data-driven Marketing Strategies"
            cls.form.length.data = 3
            cls.form.course_fee.data = 12500
            cls.form.teacher.data = cls.teacher.id

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit' and 'add' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    def test_1_create_record_handler_returns_course_created(self, mock_add, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined teacher record instead of performing its usual function. 
            mock_get.return_value = self.teacher

            # Creating a new course record through the create_record_handler function.
            created_course, result_message = create_record_handler(self.form, Course, "Course")
           
            # Creating an expected course record for comparison.
            expected_course = Course(name=self.form.name.data.strip(), length=self.form.length.data, course_fee=self.form.course_fee.data, teacher=self.teacher)

            # Asserting that db.session.get(), db.session.commit() and db.session.add() were all called exactly once
            mock_get.assert_called_once()
            mock_commit.assert_called_once()
            mock_add.assert_called_once()

            # Comparing the created course record with the expected course record.
            # This is done by comparing each column from the course table.
            records_match = compare_records(created_course, expected_course, ['name', 'length', 'course_fee', 'teacher'])

            
            # Asserting that the created course matches with the expected course
            # and checking that the result message indicates successful course creation.
            self.assertEqual(records_match, True)
            self.assertEqual((result_message), "Course Created")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit', 'add' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.rollback')
    def test_2_create_record_handler_returns_course_already_exists(self, mock_rollback, mock_add, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined teacher record instead of performing its usual function. 
            mock_get.return_value = self.teacher

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "Duplicate entry"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("Duplicate entry")

            # Creating a new course record through the create_record_handler function
            created_course, result_message = create_record_handler(self.form, Course, "Course")

            # Asserting that db.session.get(), db.session.commit(), db.session.add(), db.session.rollback() were all called exactly once
            mock_get.assert_called_once()
            mock_commit.assert_called_once()
            mock_add.assert_called_once()
            mock_rollback.assert_called_once()


            # Asserting that when attempting to create the course a duplicate error is thrown and the function
            # returns None and a message indicating the course already exists in the database.        
            self.assertEqual((created_course, result_message), (None, "This course already exists in the database"))


    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit', 'add' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.add')
    @mock.patch('backend.models.db.session.rollback')
    def test_3_create_record_handler_returns_database_error(self, mock_rollback, mock_add, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined teacher record instead of performing its usual function. 
            mock_get.return_value = self.teacher

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Creating a new course record through the create_record_handler function
            created_course, result_message = create_record_handler(self.form, Course, "Course")

            # Asserting that db.session.get(), db.session.commit(), db.session.add(), db.session.rollback() were all called exactly once
            mock_get.assert_called_once()
            mock_commit.assert_called_once()
            mock_add.assert_called_once()
            mock_rollback.assert_called_once()


            # Asserting that when attempting to create the course the function will return None and a "Database Error" message,
            # indicating that the record could not be created due to a database error.        
            self.assertEqual((created_course, result_message), (None, "Database Error"))


class TestUpdateCourseHandler(unittest.TestCase):
    teacher = None
    app_context = None

    @classmethod
    def setUpClass(cls):
        # Initializing the app context and creating a teacher record for use in test cases.
        cls.app_context = app.test_request_context()
        with cls.app_context:
            cls.teacher = Teacher(name="Olly McGuiness", age=47, gender="Male")
            cls.teacher.id = 1
            
    def setUp(self):
        # Setting up the necessary prerequisites for each test case by creating a fresh course record and a form.
        with self.app_context:
            self.form = CourseForm()
            self.form.name.data = "Data-driven Marketing Strategies"
            self.form.length.data = 3
            self.form.course_fee.data = 12500
            self.form.teacher.data = self.teacher.id
            self.created_course = Course(name=self.form.name.data.strip(), length=self.form.length.data, course_fee=self.form.course_fee.data, 
                                        teacher=self.teacher)


    def test_1_update_record_handler_returns_no_data_changed(self):
        with self.app_context:
            # Asserting that the function correctly identifies when no changes have been made
            # to the data and returns a message indicating "No Data Changed".
            self.assertEqual(update_record_handler(self.created_course, self.form, "Course", check_course_records_match), "No Data Changed")

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get' and 'commit' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    def test_2_update_record_handler_returns_course_updated(self, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined teacher record instead of performing its usual function. 
            mock_get.return_value = self.teacher

            # Modifying the length data in the form and asserting that the update function identifies
            # the change and returns a message indicating the course has been successfully updated. 
            self.form.length.data = 4
            self.assertEqual(update_record_handler(self.created_course, self.form, "Course", check_course_records_match), "Course Updated")
            
            # Asserting that db.session.commit() and db.session.get() were both called exactly once.
            mock_commit.assert_called_once()
            mock_get.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback')
    def test_3_update_record_handler_returns_course_already_exists(self, mock_rollback, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined teacher record instead of performing its usual function. 
            mock_get.return_value = self.teacher

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "Duplicate entry"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("Duplicate entry")

            # Adjusting the form data and asserting that when attempting to update the course a duplicate error is thrown 
            # and the function returns a message indicating the course already exists in the database. 
            self.form.length.data = 2
            self.assertEqual(update_record_handler(self.created_course, self.form, "Course", check_course_records_match), "This course already exists in the database")

            # Asserting that db.session.get(), db.session.commit() and db.session.rollback() were all called exactly once.
            mock_get.assert_called_once()
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()

    # Using the mock.patch decorator from the unittest.mock library. 
    # Replacing the 'get', 'commit' and 'rollback' methods of the 'db.session' object in the 'backend' module with mock objects.
    @mock.patch('backend.models.db.session.get')
    @mock.patch('backend.models.db.session.commit')
    @mock.patch('backend.models.db.session.rollback')
    def test_4_update_record_handler_returns_database_error(self, mock_rollback, mock_commit, mock_get):
        with self.app_context:
            # Configuring mock_get to return a predefined teacher record instead of performing its usual function. 
            mock_get.return_value = self.teacher

            # Setting a side effect for the mock_commit object to simulate a SQLAlchemyError with the message "General Error"
            # instead of performing its usual function.
            mock_commit.side_effect = SQLAlchemyError("General Error")

            # Setting the length field to None and asserting that when attempting to update the course 
            # a database error is thrown and the function returns a message indicating there has been an error in the database.
            self.form.length.data = None
            self.assertEqual(update_record_handler(self.created_course, self.form, "Course", check_course_records_match), "Database Error")

            # Asserting that db.session.get(), db.session.commit() and db.session.rollback() were all called exactly once.
            mock_get.assert_called_once()
            mock_commit.assert_called_once()
            mock_rollback.assert_called_once()

